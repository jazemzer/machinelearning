#R code to demonstrate Principal component analysis
#Attaching a specific library and understanding the data)
rm(list=ls(all=TRUE))

library(datasets)
attach(attitude)
names(attitude)
summary(attitude)
str(attitude)

#constructing a linear regression model

model1= lm(rating~complaints+
             privileges+
             learning+raises+
             critical+
             advance)

summary(model1)

model1<-lm(rating~complaints)
summary(model1)

#Running a PCA and analyzing the values

data <- attitude[,-c(1)]
pca_data <- princomp(data)
summary(pca_data)
plot(pca_data)
print(pca_data)

pca_data$loadings

#creating the data set with four components
predict(pca_data)
var = predict(pca_data)[,1:4]

#modeling with components

model2=lm(rating~var)
summary(model2)

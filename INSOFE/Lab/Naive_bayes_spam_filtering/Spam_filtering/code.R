# This activity consists of taking the data from 2 sources and 
# combining them to conduct the SPAM filteing analysis.
# 1 st source : a document term matrix of 64 emails and 3722 words
# related to academic conference. Below is the link for more details. 
# https://archive.ics.uci.edu/ml/datasets/DBWorld+e-mails#
# 2nd source : A list of 36 email documents that we manually collected.
# We need to first text process both the sources
# and combine them together to build spam filteing 
# algorithm. 

# Reading the Weka files use foreign 'library'
# There is no text pre-processing happend on the
# original column names (removing numbers/lower case letters, etc).
# So take the column names from the data frame 
# and create a corpus to conduct
# text processing. Map the cloumn names and 
# take the data corresponding to their column names 
# to pull the data.

# clearing R memory
rm(list=ls(all=TRUE))
# setting up the working directory
setwd("D:/INSOFE/Text_mining/Demo/Spam_filtering")

# Installing the required libraries
library(foreign) # To read Weka files
library(tm) # To handle the text processing 
library(gtools) # To combine matrices/data frames consisting of 


# Reading the data in text format source-2
ham.docs<-Corpus(DirSource("D:/INSOFE/Text_mining/Demo/Spam_filtering/Data/Non_spam"), readerControl = list(reader=readPlain)) 
spam.docs<-Corpus(DirSource("D:/INSOFE/Text_mining/Demo/Spam_filtering/Data/Spam"), readerControl = list(reader=readPlain)) 

# 90% sparse - meaning that 90% of its cells are 
# either not filled with data or are zeros.

# Text pre processing function
tm_process <- function(docs,spar_percnt){
  #Performing text processing steps:
  # Stip any numbers from a text document
  docs <- tm_map(docs, removeNumbers)
  # Remove punctuation marks from a text document
  docs <- tm_map(docs, removePunctuation)
  #Strip extra whitespace from a text document
  docs <- tm_map(docs , stripWhitespace)
  #Convert into lower case 
  docs <- tm_map(docs, tolower)
  # Remove a set of words from a text document
  docs <- tm_map(docs, removeWords, stopwords("english")) 
  #Stem words in a text document using Porter's stemming algorithm
  docs <- tm_map(docs,stemDocument, language ="english")
  #Constructs or coerces to a document-term matrix
  dt_matrix <-DocumentTermMatrix(docs) 
  # Remove sparse terms from a document-term matrix
  dt_matrix <- removeSparseTerms(dt_matrix, spar_percnt)
  # Data Preparation for Classification Model:
  #Store  document-term matrix in a separate data object
  matrix=data.frame(inspect(dt_matrix))
  }
# Applying the text processing on the Spam and ham corpuses
ham_dtm <- tm_process(ham.docs,0.8)
spam_dtm <- tm_process(spam.docs,0.8)

### Adding the class lables
# 16 spam documents
ham_dtm$CLASS <- rep(1,nrow(ham_dtm))
names(ham_dtm)
# 20 spam documents
spam_dtm$CLASS <- rep(0,nrow(spam_dtm))
names(spam_dtm)
### Adding the document term matrices of both ham and spam matrices
matrix_result <- smartbind(ham_dtm,spam_dtm,fill=0)
table(matrix_result$CLASS)
rm(ham_dtm,spam_dtm)
write.csv(matrix_result,"unstr_data.csv",row.names=F)
# Reading the weka file into R 
# This below step take approx. 45 secs on 8 gb ram machine
data <- read.arff("Data/Spam_data-From-UCI/dbworld_bodies_stemmed.arff")
# To check the data in CSV format
write.csv(data,"uci_data.csv",row.names=F)
# As we no need to do text processing for the target variable, 
# let us separate the target variable and storing in another vector
CLASS <- data$CLASS
# spam - 0 : 35 records
# ham - 1 : 29 records
table(data$CLASS)
# removing the target variable
data1 <- subset(data,select=-c(CLASS))

# Taking the column names into a vector
# applying the pre-processing steps

docs_names <- paste(names(data1),collapse=" ")
# converting into corpus
docs.names <- Corpus(DataframeSource(data.frame(docs_names)))
# removing numbers
names_docs <- tm_map(docs.names, removeNumbers)
inspect(names_docs)
# Remove punctuation marks from a text document
names_docs <- tm_map(names_docs, removePunctuation)
inspect(names_docs)
#Strip extra whitespace from a text document
names_docs <- tm_map(names_docs , stripWhitespace)
inspect(names_docs)
#Convert into lower case 
names_docs <- tm_map(names_docs, tolower)
# Remove a set of words from a text document
# You can define your own list of words
mystopwords <- c(stopwords('english'),"academ","academi","academia","academico")
names_docs <- tm_map(names_docs, removeWords, mystopwords)   
#names_docs <- tm_map(names_docs, removeWords, stopwords("english")) 
inspect(names_docs)
#Stem words in a text document using Porter's stemming algorithm
names_docs <- tm_map(names_docs,stemDocument, language ="english")
inspect(names_docs)
dt_matrix <-DocumentTermMatrix(names_docs) 
matrix <- data.frame(inspect(dt_matrix))
sorted_1gram <- sort(colSums(matrix),decreasing=TRUE)
Sorted_1df <- data.frame(word = names(sorted_1gram),freq=sorted_1gram)
write.csv(Sorted_1df,"uci_data_sorted.csv",row.names=F)
common_vars <- names(data1)%in%names(matrix)
# common variables
table(common_vars)
# Taking the column names from the original data
weka_final_data <- data1[common_vars]
# Adding the target variable
weka_final_data <- cbind(weka_final_data,CLASS)

write.csv(weka_final_data,"weka_final_data.csv",row.names=F)
#### 
### Adding the data from weka files to email data
table(weka_final_data$CLASS)

final_data <- smartbind(weka_final_data,matrix_result,fill=0)
table(final_data$CLASS)
rm(data,matrix_result)
## adding the class label 
write.csv(final_data,"final_data.csv",row.names=F)
str(final_data)
# 
final_data1 <- apply(final_data,2,function(x) as.factor(x))
final_data1 <- data.frame(final_data1)

rows=seq(1,nrow(final_data1),1)
set.seed(123)
trainRows=sample(rows,nrow(final_data1)*0.7)
set.seed(123)
testRows=rows[-(trainRows)]

train = final_data1[trainRows,] 
test=final_data1[testRows,] 

# Checking the class disribution
table(train$CLASS)
table(test$CLASS)

# Applying naive bayes model on the train data
library(e1071)
model = naiveBayes(CLASS~., data = train,threshold = 0.001)
model
pred_train = predict(model, train)
pred_test = predict(model, test)

#Confusion matrix with train data
# 97.14% Accuracy
# 95.12% Recall 
# Take Spam as Positive and Not spam as negative
# If the email is actually spam and we predcit as not spam , then it is false positive
# so the error metric is  Precision.
conf.nb <- table(pred_train, train$CLASS)
accuracy.nb <- sum(diag(conf.nb))/nrow(train) * 100
recall<- (conf.nb[1,1])/(conf.nb[1,1]+conf.nb[1,2])*100
sprintf("The accuracy the model using naive bayes is : %0.2f", accuracy.nb)
sprintf("The recall of the model using naive bayes is : %0.2f", recall)

#Confusion matrix with test data
# 83.33 Accuracy
# 82.65 Recall 
conf.nb <- table(pred_test, test$CLASS)
accuracy.nb <- sum(diag(conf.nb))/nrow(test) * 100
recall.nb<- (conf.nb[1,1])/(conf.nb[1,1]+conf.nb[1,2])*100
save.image("spam_filteing.Rdata")
sprintf("The accuracy the model using naive bayes is : %0.2f", accuracy.nb)
sprintf("The recall of the model using naive bayes is : %0.2f", recall)

##### SVM 
x = subset(train, select = -CLASS)
y  =  train$CLASS
model  =  svm(x,y, method = "C-classification", kernel = "linear")
pred_train  =  predict(model, x)
conf.svm <- table(pred_train, train$CLASS)
accuracy.svm <- sum(diag(conf.svm))/nrow(train) * 100
recall.svm<- (conf.svm[1,1])/(conf.svm[1,1]+conf.svm[1,2])*100
sprintf("The accuracy the model using svm on train data is : %0.2f", accuracy.svm)
sprintf("The recall of the model using svm on train data is : %0.2f", recall.svm)

# 83.33 Accuracy
# 86.67 Recall 

x.test <- subset(test,select=-CLASS)
pred_test  =  predict(model, x.test)
conf.svm <- table(pred_test, test$CLASS)
accuracy.svm <- sum(diag(conf.svm))/nrow(test) * 100
recall.svm<- (conf.svm[1,1])/(conf.svm[1,1]+conf.svm[1,2])*100
sprintf("The accuracy the model using SVM is : %0.2f", accuracy.svm)
sprintf("The recall of the model using SVM is : %0.2f", recall.svm)

### NB                  # SVM
# 83.33 Accuracy  # 83.33 Accuracy
# 82.65 Recall    # 86.67 Recall 

rm(list=ls(all=TRUE))
setwd("D:/ML/Murthy/Naive_bayes1/data")
flight <- read.csv("FlightDelays.csv",header=TRUE,sep=",")

head(flight)

flight = flight[,1:13]

str(flight)

length(unique(flight$FL_DATE))

flight$Weather = as.factor(flight$Weather)
flight$DAY_WEEK = as.factor(flight$DAY_WEEK)
flight$DAY_OF_MONTH = as.factor(flight$DAY_OF_MONTH)
flight$Scheduled_Time = as.factor(as.numeric(cut(flight$CRS_DEP_TIME, 10)))
flight$Departure_Time = as.factor(as.numeric(cut(flight$DEP_TIME, 10)))
flight$Dist = as.factor(as.numeric(cut(flight$DISTANCE, 10)))
flight$Flight_No = as.factor(flight$FL_NUM)

drops = c('FL_NUM','CRS_DEP_TIME','DEP_TIME','DISTANCE')
flight = flight[,!(names(flight) %in% drops)]


smp_size <- floor(0.75 * nrow(flight))
set.seed(123)
train_ind <- sample(seq_len(nrow(flight)), size = smp_size)

trainData <- flight[train_ind, ]
testData <- flight[-train_ind, ]

# Applying Naive Bayes
library(e1071)
model = naiveBayes(Flight.Status~., data = trainData)
model

head(testData)

pred = predict(model, testData) #, type="raw")

conf = table(testData$Flight.Status,pred)
TP = conf[2,2]
FP = conf[1,2]
FN = conf[2,1]
recall = TP /(TP + FN)
precision = TP/(TP+FP)
F1 = 2*(precision * recall)/(precision + recall)

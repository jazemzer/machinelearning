Your Excellency, President Mahmoud Abbas,

President of the Palestinian National Authority,

Chairman of the Palestinian Liberation Organisation

It is, indeed, a great pleasure to extend a warm welcome to Your Excellency, and to the distinguished members of your delegation on your State Visit to India.

Your visit is another important step in further strengthening the time tested friendship, solidarity and co-operation that have always characterised relations between India and Palestine.

Distinguished guests,

India's commitment to Palestine was given voice by none less than Mahatma Gandhi in the days of India's own struggle for independence. As early as in 1936, 'Palestine Day', to mark our solidarity, was observed in India. In the early years of independent India, our policy and support for the Palestinian cause was consolidated under the leadership of our first Prime Minister, Pandit Jawaharlal Nehru. We were the first country outside the Arab world to recognize the Palestine Liberation Organization, in 1975, as the sole and legitimate representative of the Palestinian people. In March 1980, we accorded recognition to the PLO Office in New Delhi - just two days prior to the first official visit of Chairman Yasser Arafat to India. Soon after that, India recognized the State of Palestine in 1988 - after which we opened our Representative Office to the State of Palestine in 1996.

Your Excellency,

India has been consistent and persevering in its commitment to the Palestinian cause. In the United Nations, the Non Aligned Movement, and in other international fora, India has always supported the Palestinian people in pursuing their legitimate aspiration for a sovereign, independent and viable State. India wishes to see your realisation of a State of Palestine, with East Jerusalem as its capital, existing within secure and recognised borders side by side and in peace with Israel. Towards this end, India has underlined its support for the Arab Peace Initiative and the Quartet Road Map; we have repeatedly called for the implementation of relevant resolutions of the United Nations Security Council and General Assembly.

Your Excellency, I heartily commend the indefatigable spirit of the Palestinian people and take this opportunity to reaffirm India's principled and unwavering support to their just cause. We have always considered the conflict in West Asia to be political in nature and therefore we believe that it cannot be resolved by force.

Excellency, India fully supports your efforts to be a member of the United Nations. Our envoys and representatives have worked together to achieve the goal of Palestine's membership of UNESCO. Very recently, as a member of the heritage committee, India supported Palestine in having the holy Church of Nativity in Bethlehem recognised as a World Heritage Site.

India has been proud to assist the Palestinian people in their continuing endeavour towards nation-building. We have seen, with satisfaction, the positive outcome of grants and resources shared by us over the decades for your developmental projects, creation of educational and training facilities, capacity building and humanitarian relief.

Excellency, I was also happy to learn that you have inaugurated the new Palestinian Embassy building in New Delhi earlier this evening. This is a proud moment in our diplomatic history - and your Embassy will ever be a monument to the co-operation and enduring friendship between our two countries and our peoples.

Your visit to India has yet again been purposeful and productive, an occasion to review our bilateral agenda , strengthen our co-operation for mutual benefit and to renew our commitment to achieving our common goals in the years to come.

With these words, Excellencies, Ladies and Gentlemen, I request you to join me in raising a toast:

-	to the good health and success of His Excellency President Mahmoud Abbas, President of the Palestinian National Authority;

-	to the prosperity and progress of the State of Palestine and its friendly people; and

-	to the abiding friendship and co-operation between India and Palestine.

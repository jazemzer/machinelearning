rm(list=ls(all=TRUE))
setwd("D:/RegBatch/CPEE/NN")

library(neuralnet)

library(dummies)
library(vegan)


#Bank problem
bankdata=read.csv(file="UniversalBank.csv", 
                  header=TRUE, sep=",")
bankdata2=subset(bankdata, 
                 select=-c(ID,ZIP.Code)) # to remove the columns ID & ZIP Code from the data
Education=dummy(bankdata2$Education)
bankdata3=subset(bankdata2,select=-c(Education)) 
bankdata4=cbind(bankdata3,Education)
bankdata5=decostand(bankdata4,"range") # to standardize the data using 'Range' method

set.seed(123) # to get same data in each time
train = sample(1:5000,3000) # to take a random sample of  60% of the records for train data 
bankdata_train = bankdata5[train,] 
test = (1:5000) [-train] # to take a random sample of  40% of the records for test data 
bankdata_test = bankdata5[test,] 

table(bankdata5$Personal.Loan)
table(bankdata_train$Personal.Loan)
table(bankdata_test$Personal.Loan)

rm(bankdata2, bankdata3, bankdata4, bankdata5, Education, test, train)

nn=neuralnet(Personal.Loan~ 
               Age+Experience+
               Income+Family+
               CCAvg+Mortgage+
               Securities.Account+CD.Account+Online+CreditCard+Education1+Education2+
               Education3, 
             data=bankdata_train,
             hidden=c(3,2))

out <- cbind(nn$covariate,
             nn$net.result[[1]])

dimnames(out) = list(NULL,c ("Age","Experience","Income","Family","CCAvg","Mortgage","Securities.Account","CD.Account","Online","CreditCard","Education1"," Education2", "Education3","nn-output"))
head(out) # to view top records in the data set
plot(nn)



#Data preparation for classification matrix
p=as.data.frame(nn$net.result)
colnames(p)="pred"
pred_class <- factor(ifelse(p$pred > 0.5, 1, 0))
a <- table(pred_class, 
           bankdata_train$Personal.Loan)
recall <- a[2,2]/(a[2,1]+a[2,2])*100
recall

#retrieving required columns from the data
test_data2=subset(bankdata_test,
                  select=-c(Personal.Loan))
new.output <- compute(nn,
                      covariate=test_data2)
p=as.data.frame(new.output$net.result)
colnames(p)="pred"
pred_class <- factor(ifelse(p$pred > 0.5, 1, 0))
a <- table(pred_class,bankdata_test$Personal.Loan)
recall <- a[2,2]/(a[2,1]+a[2,2])*100
recall

#Play with different node structures (3), (2,2), (4,3)